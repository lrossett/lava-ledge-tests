# Lava LEDGE Tests

This repository aspires to contain the sum of tests used across
the Linaro Edge group (TRS, Cassini, etc).

## License

[MIT](./LICENSE) is the default license being used in this
repository unless it needs to use a file from another project,
the license of such files will be preserved in this case.
